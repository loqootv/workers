#!/usr/bin/env python

from datetime import datetime, timedelta
import math

def reltime(date, compare_to=None, at='@'):
    def ordinal(n):
        if 10 <= n % 100 < 20:
             return str(n) + 'th'
        else:
             return str(n) + {1 : 'st', 2 : 'nd', 3 : 'rd'}.get(n % 10, "th")

    compare_to = compare_to or datetime.now()
    if date > compare_to:
        return NotImplementedError('reltime only handles dates in the past')
    diff = compare_to - date
    if diff.seconds < 60 * 60 * 8:
         days_ago = diff.days
    else:
         days_ago = diff.days + 1
    months_ago = compare_to.month - date.month
    years_ago = compare_to.year - date.year
    weeks_ago = int(math.ceil(days_ago / 7.0))
    hr = date.strftime('%I')
    if hr.startswith('0'):
        hr = hr[1:]
    wd = compare_to.weekday()
    if date.minute == 0:
        time = '{0}{1}'.format(hr, date.strftime('%p').lower())
    else:
        time = '{0}:{1}'.format(hr, date.strftime('%M%p').lower())
    if days_ago == 0:
        datestr = 'today {at} {time}'
    elif days_ago == 1:
        datestr = 'yesterday {at} {time}'
    elif (wd in (5, 6) and days_ago in (wd+1, wd+2)) or \
            wd + 3 <= days_ago <= wd + 8:
         datestr = 'last {weekday} {at} {time} ({days_ago} days ago)'
    elif days_ago <= wd + 2:
         datestr = '{weekday} {at} {time} ({days_ago} days ago)'
    elif years_ago == 1:
         datestr = '{month} {day}, {year} {at} {time} (last year)'
    elif years_ago > 1:
         datestr = '{month} {day}, {year} {at} {time} ({years_ago} years ago)'
    elif months_ago == 1:
         datestr = '{month} {day} {at} {time} (last month)'
    elif months_ago > 1:
         datestr = '{month} {day} {at} {time} ({months_ago} months ago)'
    else:
         datestr = '{month} {day} {at} {time} ({days_ago} days ago)'
    return datestr.format(time=time,
                         weekday=date.strftime('%A'),
                         day=ordinal(date.day),
                         days=diff.days,
                         days_ago=days_ago,
                         month=date.strftime('%B'),
                         years_ago=years_ago,
                         months_ago=months_ago,
                         weeks_ago=weeks_ago,
                         year=date.year,
                         at=at)
