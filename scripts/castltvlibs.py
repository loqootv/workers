import sys, time
from Quartz.CoreGraphics import *

def mouseEvent(type, posx, posy):
  theEvent = CGEventCreateMouseEvent(None, type, (posx,posy), kCGMouseButtonLeft);
  CGEventPost(kCGHIDEventTap, theEvent);

def mouseclickdn(posx,posy):
  mouseEvent(kCGEventLeftMouseDown, posx,posy)

def mouseclickup(posx,posy):
  mouseEvent(kCGEventLeftMouseUp, posx,posy)

def clickupDown(x,y):
    mouseclickdn(x,y)
    mouseclickup(x,y)

ourEvent = CGEventCreate(None)
currentpos = CGEventGetLocation(ourEvent)
