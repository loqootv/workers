#!/usr/bin/env bash

launchctl unload ~/Library/LaunchAgents/$1.plist;
launchctl load ~/Library/LaunchAgents/$1.plist;
launchctl start $1
