#!/usr/bin/env python
import string, cgi, datetime, time, os, urlparse as ups, sys
from logentries import LogentriesHandler
import logging
from time import sleep
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import subprocess
import requests
import json
import redis
import shortuuid
import isodate
import threading
from firebase import Firebase
import mongodb 
from utils.unsh import unshorten_url
from urlparse import urlparse
from utils.bin import *
from parseDb import *
from contentProviders import *
from os import curdir
from os.path import join as pjoin

youtubeTVshow = None
contentTypes= {"1":"youtube", "2":"soundcloud", "3":"instagram", "4":"hulu", "5":"vine"}
INSTAACCESSTOKEN = os.environ['INSTAACCESSTOKEN']
dd = os.environ['MONGOLABKEY']
PARSEAPPID = os.environ['PARSEAPPID']
PARSEMASTERKEY = os.environ['PARSEMASTERKEY']
PARSERESTAPIKEY = os.environ['PARSERESTAPIKEY']
time = datetime.date.today().strftime("%A %B %d %Y")
a1 = contentProviders()

class CHANNEL (BaseHTTPRequestHandler):

    store_path = pjoin(curdir, 'store.json')
    show = Show()
    sharer = Sharer()
    creator = Creator
    YTAPIKEY = "AIzaSyBl_vevuT3hzLCrK4rysf3XMPZtt5LxnWg"

    def headers_301(self):
	self.send_response(301)
	self.send_header('Location','http://loqoo.tv/1')
	self.end_headers()
    
    def parseYoutubeUrl(self,url):
        m = ups.urlparse(url)
	m1 = ups.parse_qs(m.query)['v']
	m1a  = str(m1)[2:-2]    
	return m1a
    

    def hulu_handler():
	    pass

    def soundcloud_handler(self, url, level, sharer):
	 scID, scDURATION, scUSER_CITY, scUSER_COUNTRY, scTITLE, scUSER_LINK, scUSER_PIC, scARTWORK, scDESCRIP, scDOWNLOAD, scLIKES, scSPINS = scINFO(url)  
	 scSECONDS = float(scDURATION) / 1000

	 loqootvMSG = {'sentByEmail': "", 'contentType': contentTypes["2"], 'contentDuration': scSECONDS, 'contentCity': scUSER_CITY, 'contentCountry': scUSER_COUNTRY, 'contentTitle': scTITLE, 'contentArtUrl': scARTWORK, 'contentDesc': scDESCRIP, 'contentId': scID, 'contentProfile': scUSER_LINK, 'contentProfileAvatar': scUSER_PIC, 'contentUrl': url, 'contentLikes': scLIKES, 'contentViews': scSPINS}
	 
	 mongodb.queueInsert(loqootvMSG)
	 del loqootvMSG["_id"]
	 loqooTVSHOW = json.dumps(loqootvMSG)
         print loqooTVSHOW
	 r = redis.Redis("localhost")
	 r.rpush("tv.loqoo.1.queue", loqooTVSHOW)


    def instagram_handler():
	    pass

     
    def do_HEAD(self):
        print "send header"
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_AUTHHEAD(self):
        print "send header"
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"Test\"')
        self.send_header('Content-type', 'text/html')
	self.end_headers()  

    def parseYoutubeUrl(self, url):                                                                                                                                               
          m = ups.urlparse(url)
          m1 = ups.parse_qs(m.query)['v']
          m1a  = str(m1)[2:-2]    
          return m1a     
    def do_POST(self):
        if self.path == '/fb':
            length = self.headers['content-length']
            data = self.rfile.read(int(length))

            with open(self.store_path, 'a') as fh:
		r = json.dump(data,fh)
            print r

            self.send_response(200) 	
    def do_GET(self):

        qs = {}
        path = self.path
        print path
        if path.endswith('robots.txt'):
          self.send_response(500)
          self.send_header('Connection', 'close')
          return
        if '?' in path:
          path, tmp = path.split('?', 1)
          qs = ups.parse_qs(tmp)

	if 'fb' in path:
	   self.send_response(200)
	   self.end_headers()
	   ok = qs.get('hub.mode')
	   ok2 = str(qs.get('hub.challenge'))
	   ok3 = qs.get('hub.verify_token')
	   fin = str(ok2)[2:-2]
	   self.wfile.write(fin)
	   print r
	   print ok, ok3, ok2 
	   headers = self.headers.get('Host')
	   print headers

	#if 'thumbsdown':
        if 'txtmsg' in path:
	 if self.headers.getheader('Authorization') == None:
            self.do_AUTHHEAD()
            self.wfile.write('no auth header received')
            pass
         elif self.headers.getheader('Authorization') ==  'Basic dGVzdDpvaw==':
            self.send_response(200)
            self.send_header('Access-Control-Allow-Origin', '*')
            self.send_header('Content-Type:', 'application/json; charset=UTF-8')
            self.end_headers()        
	    self.wfile.write(json.dumps({u'response':True}))
            pass
         else:
            self.do_AUTHHEAD()
            self.wfile.write(self.headers.getheader('Authorization'))
            self.wfile.write('not authenticated')
            pass

	#def urlParameters():
	#get_show_handler(self, u):

        if 'ads' in path:
	 #self.send_response(200)
	 self.headers_301()
	 ID, LTV, EMAIL, TIME, userID, URL, url1, level1i, sharer1   = map(\
			 qs.get, ['vID', 'channelID', 'uEmail', 'uID', 'utime','contentUrl', 'url', 'level', 'sharer'])
	 channelID, uEmail, ytID, uTime, uID, url1a, url, leveli, sharer  = map(\
    	 lambda x : str(x)[2:-2], [LTV, EMAIL, ID, TIME, userID, URL, url1, level1, sharer1])
	 meta = youtube(url)
	 print meta
         mongodb.queueInsert(meta) 
         del meta["_id"]
	 youtubeTVshowJsonString = json.dumps(meta)
	 r = redis.Redis("localhost")
	 ok = r.lpush("tv.loqoo.1.queue", youtubeTVshowJsonString)

	if 'admin' in path:
	 self.headers_301()
	 CHANNELID1, EMAIL, TIME, c1  = map(\
			 qs.get, ['channelID', 'uEmail','utime','c'])
	 channelID, uEmail, uTime, c = map(\
    	 lambda x : str(x)[2:-2], [CHANNELID1, EMAIL, TIME, c1])
	 for i in (channelID, uEmail, uTime, c):
		     	     print i
	 handlers = { 1: restartltv(), 2: stopltv(), 3: delqueue, 4: delads, 5: restartfeeda, 6: restartstream, 7: delrtq, 8: delvipq, 10: stopapi, 11: playad1, 12: playad2,  13: playad3, 14: playad4, 15: playad5, 16: killinsta }
         handler = handlers.get(c)                                                            
         handler()

	if 'shows' in path:
	 self.headers_301()
	 ID, LTV, EMAIL, TIME, userID, url1, level1, who1, show1  = map(\
			 qs.get, ['vID', 'channelID', 'uEmail', 'uID', 'utime','url', 'level','sharer', show])
	 channelID, uEmail, ytID, uTime, uID, url, level, shareri, show = map(\
    	 lambda x : str(x)[2:-2], [LTV, EMAIL, ID, TIME, userID, url1, level1, who1, show1])
	 for i in (channelID, uEmail, ytID, uTime, uID, url, level, sharer, show):
		     	     print i
         handlers = { 'www.youtube.com': cp.youtube_handler, 'hulu.com': cp.hulu_handler, 'soundcloud.com': cp.soundcloud_handler, 'instagram.com': cp.instagram_handler}
	 urlParsed = urlparse(url)
         handler = handlers.get(urlParsed.netloc)                                                            
         handler(url, level, sharer, show)
         		
        if 's3' in path:
         self.headers_301()
	 URL1 = map(\
			 qs.get, [url])
	 url  = map(\
    	 lambda x : str(x)[2:-2], [URL1])
	 print url 
	 urlString = json.dumps(url)
	 print urlString
	 r = redis.StrictRedis(host='pub-redis-13469.us-east-1-1.2.ec2.garantiadata.com', port=13469, db=0, password="zxcvbnM1")
	 q = r.lpush("tv.loqoo.channel", urlString)
	 print q
        if 'youtubeTV' in path:
         self.headers_301()
	 ID, LTV, EMAIL, TIME, userID, URL, YT   = map(\
			 qs.get, ['vID', 'channelID', 'uEmail', 'uID', 'utime','contentUrl', 'url'])
	 channelID, uEmail, ytID, uTime, uID, contentUrl, url  = map(\
    	 lambda x : str(x)[2:-2], [LTV, EMAIL, ID, TIME, userID, URL, YT])
	 print url 
	 if url != None:
	   ytID = self.parseYoutubeUrl(url) 
	 print ytID	
	 for i in (channelID, uEmail, ytID, uTime, uID, contentUrl):
    	     print i
	 youtubeTVshow1 = self.youtubeTV(ytID,channelID, uEmail, uTime, uID, contentUrl, URL, YT)
	 print youtubeTVshow1
         mongodb.queueInsert(youtubeTVshow1) 
	 #parse.
         del youtubeTVshow1["_id"]
	 youtubeTVshowJsonString = json.dumps(youtubeTVshow1)
	 r = redis.Redis("localhost")
	 r.lpush("tv.loqoo.1.queue", youtubeTVshowJsonString)
        if 'instagramTV' in path:
	 self.send_response(200)
         instaShortcode, LTV, EMAIL, TIME, userID, URL  = map(\
         qs.get, ['instaID', 'channelID', 'uEmail', 'uID', 'utime','contentUrl'])

         channelID, uEmail, instagramShortcode, uTime, uID, contentUrl = map(\
         lambda x : str(x)[2:-2], [LTV, EMAIL, instaShortcode, TIME, userID, URL])
         for i in (channelID, uEmail, instagramShortcode, uTime, uID, contentUrl):
             print i
	 instaSTEP2 = requests.get("http://api.instagram.com/oembed?url=http://instagr.am/p/%s/"% instagramShortcode).json()
	 instaMeID = instaSTEP2['media_id']
	 instaINFO = requests.get("https://api.instagram.com/v1/media/%s?access_token=%s" % instaMeID, INSTAACCESSTOKEN).json()
	 loqootvMSG = {'userEmail': uEmail, 'toChannel': channelID, 'timeSent': str( datetime.datetime.now().time() ) + " " + str( datetime.date.today().strftime("%A %B %d %Y") ), 'tvshowID': shortuuid.uuid()}
	 for msgkey, subdict, instakey in (('profilePIC', 'user', 'profile_picture'), 
                            ('contentCreator', 'user', 'username'),
                            ('contentDesc', 'caption', 'text'),
			    ('contentUrl','images', 'standard_resolution')):
    	     try:
        	 loqootvMSG[msgkey] = instaINFO['data'][subdict][instakey]
    	     except TypeError:
        	 loqootvMSG[msgkey] = ""
	 loqooTVSHOW = json.dumps(loqootvMSG)
	 print loqooTVSHOW
	 r  = redis.Redis("localhost")
	 r.rpush("1.loqoo.tv.queue", loqooTVSHOW) 
	 nowp  = r.rpop("1.loqoo.tv.upnext.pic.queue")
	 print tester
	 np  = json.loads(nowp)
	 print np['msgBODY']
	 print np['profilePIC']
	 print np['userNAME']
	 print np['msgBODY']
	 print np['mainCONTENT']
         subprocess.Popen('./killltv', shell=True)
         sleep(1)
         subprocess.Popen('./instaSTICKIES.py %s'% instaURL, shell=True)
         #subprocess.Popen('./test.py %s'% instaURL, shell=True) 

	if 'VIP' in path:
	 self.send_response(301)
	 self.send_header('Location','http://loqoo.tv/')
	 self.end_headers()
	 ID, LTV, EMAIL, TIME, userID, URL,   = map(\
         qs.get, ['vID', 'channelID', 'uEmail', 'uID', 'utime', 'contentUrl'])
	 channelID, uEmail, ytID, uTime, uID, contentUrl,  = map(\
	 lambda x : str(x)[2:-2], [LTV, EMAIL, ID, TIME, userID, URL])
         for i in (channelID, uEmail, uTime, uID, contentUrl):
             print i
          
        if 'soundcloudTV' in path:
	 self.send_response(301)
	 self.send_header('Location','http://loqoo.tv/')
	 self.end_headers()
	 ID, LTV, EMAIL, TIME, userID, URL,   = map(\
         qs.get, ['vID', 'channelID', 'uEmail', 'uID', 'utime', 'contentUrl'])
	 channelID, uEmail, ytID, uTime, uID, contentUrl,  = map(\
	 lambda x : str(x)[2:-2], [LTV, EMAIL, ID, TIME, userID, URL])
         for i in (channelID, uEmail, uTime, uID, contentUrl):
             print i

	 scID, scDURATION, scUSER_CITY, scUSER_COUNTRY, scTITLE, scUSER_LINK, scUSER_PIC, scARTWORK, scDESCRIP, scDOWNLOAD, scLIKES, scSPINS = scINFO(u)  
	 scSECONDS = float(scDURATION) / 1000

	 loqootvMSG = {'sentByEmail': uEmail, 'sentToChannel': channelID, 'contentType': contentTypes["2"], 'contentDuration': scSECONDS, 'contentCity': scUSER_CITY, 'contentCountry': scUSER_COUNTRY, 'contentTitle': scTITLE, 'contentArtUrl': scARTWORK, 'contentDesc': scDESCRIP, 'contentId': scID, 'contentProfile': scUSER_LINK, 'contentProfileAvatar': scUSER_PIC, 'contentUrl': contentUrl, 'contentLikes': scLIKES, 'contentViews': scSPINS}
	 
	 mongodb.queueInsert(loqootvMSG)
	 del loqootvMSG["_id"]
	 loqooTVSHOW = json.dumps(loqootvMSG)
         print loqooTVSHOW
	 r = redis.Redis("localhost")
	 r.lpush("1.loqoo.tv.queue", loqooTVSHOW)
        #if 'twitterTV' in path:
        #if 'vineTV' in path:
        #if 'wwwTV' in path:

 
    def log_request(self, code=None, size=None):
        print('Request')
    def log_message(self, format, *args):
        print('Message')

if __name__ == "__main__":
    try:
        server = HTTPServer(('0.0.0.0', 8000), CHANNEL)
        print('Started http server')
        #	log.warn("MY LTV is ON! )
        server.serve_forever()
    except KeyboardInterrupt:
        print('^C received, shutting down server')
        log.warn("LTV OFF")
        server.socket.close()
