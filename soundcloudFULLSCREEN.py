#!/usr/bin/env python

import sys
from PySide import QtCore, QtGui, QtOpenGL
from PySide.QtWebKit import QGraphicsWebView
 
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
  
    width = 900
    height = 900 
 

    scene = QtGui.QGraphicsScene()
    
    view = QtGui.QGraphicsView(scene)
    
    #scene.setSceneRect(50,50,50,50)
    scene.setBackgroundBrush(QtCore.Qt.black)
    view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    view.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
    webviewMAIN = QGraphicsWebView()
    #webviewTOP = QGraphicsWebView()
    #webviewBOTTOM = QGraphicsWebView()
    #webviewRIGHT = QGraphicsWebView()
    url = sys.argv[1]
#    url2 = sys.argv[2]
 #   url3 = sys.argv[3]
    #url4 = sys.argv[4]
    #url5 = sys.argv[5]
    #webview.setResizesToContents(enabled)
  # webviewMAIN.resize(width, height)
    webviewMAIN.load(QtCore.QUrl("%s" % url))
    #webviewTOP.setPos(0,0)
    webviewMAIN.setPos(-0,-3)
   # webviewBOTTOM.setPos(-60,900)
   
   #webviewRIGHT.setPos(1180,-3)
    #webviewTOP.load(QtCore.QUrl("%s" % url2))
 #   webviewBOTTOM.load(QtCore.QUrl("%s" % url3))
   # webviewRIGHT.load(QtCore.QUrl("%s" % url2))
    webviewMAIN.setZoomFactor(3.75)
    webviewMAIN.resize(1425, 555)
    #webviewRIGHT.resize(715, 1053)
   # webviewBOTTOM.resize(1225, 150)

    scene.addItem(webviewMAIN) 
    #scene.addItem(webviewTOP) 
    #scene.addItem(webviewBOTTOM) 
    #scene.addItem(webviewRIGHT) 
    view.resize(width, height)
    view.showFullScreen()
    view.raise_()
    view.show()
 
    sys.exit(app.exec_())
