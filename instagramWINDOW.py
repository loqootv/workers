#!/usr/bin/env python

import sys
from PySide import QtCore, QtGui
from PySide.QtWebKit import QGraphicsWebView, QWebView
 
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
  
    width = 1200
    height = 1200 
 

    scene = QtGui.QGraphicsScene()
    
    view = QtGui.QGraphicsView(scene)
    
    scene.setSceneRect(500,400,500,500)
    scene.setBackgroundBrush(QtCore.Qt.black)
    view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    view.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
    webview = QtWebKit.QWebView()
    webview.setAttribute(QtCore.Qt.WA_TranslucentBackground)
    url = sys.argv[1]
    #webview.setResizesToContents(enabled)
    webview.resize(width, height)
    webview.load(QtCore.QUrl("%s" % url))
    webview.setZoomFactor(1.5)
    
    scene.addItem(webview) 
    view.resize(width, height)
    view.showFullScreen()
    view.raise_()
    view.show()
 
    sys.exit(app.exec_())