#!/usr/bin/env python

import pymongo
from pymongo import MongoClient

connection = MongoClient("mongodb://ds047448.mongolab.com", 47448)
db = connection["loqootv_1"]
db.authenticate("loqootv", "loqootv")
queue = db.queue
played = db.played
ads = db.ads

def queueInsert(document):
	queue.insert(document)

def playedInsert(document):
	played.insert(document)

def adsInsert(document):
	ads.insert(document)

def txt4app(document):
	txt4app.insert(document)

adsInsert({"ok":"ok"})
