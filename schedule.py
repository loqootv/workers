#!/usr/bin/env python

from parse_rest.connection import register
from parse_rest.datatypes import Object
from parse_rest.query import *
import os, json, httplib

PARSEAPPID = os.environ['PARSEAPPID']
PARSERESTAPIKEY = os.environ['PARSERESTAPIKEY']

register(PARSEAPPID, PARSERESTAPIKEY)

class twitterSharer(Object):
    def addtoDB(self,xtScreename, xtId, xtProfileImageUrl, xtLocation, xtTimezone, xtLang, xtBackgroundImageUrl, xltvFollower, xtVerified, xtDescription, xtFollowerCount):
	    try:
		    oid = twitterSharer.Query.get(tScreename=xtScreename)
		    oId = str(oid)[15:-1]
		    connection = httplib.HTTPSConnection('api.parse.com', 443)
                    connection.connect()
		    connection.request('PUT', '/1/classes/twitterSharer/%s'% oId, json.dumps({
			    "tId": xtId,
			    "tProfileImageUrl": xtProfileImageUrl,
			    "tLocation": xtLocation,
			    "tTimezone": xtTimezone,
			    "tLang": xtLang,
			    "tBackgroundImageUrl": xtBackgroundImageUrl,
			    "ltvFollower": xltvFollower,
			    "tVerified": xtVerified,
			    "tDescription": xtDescription,
			    "tFollowerCount": xtFollowerCount
			  }),  {
			    "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			    "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			    "Content-Type": "application/json"
			  })
	            result = json.loads(connection.getresponse().read())
		    return oId
            except QueryResourceDoesNotExist:
                    new = twitterSharer(tScreename=xtScreename,tId=xtId, tProfileImageUrl=xtProfileImageUrl, tLocation=xtLocation, tLang=xtLang, tBackgroundImageUrl=xtBackgroundImageUrl, ltvFollower=xltvFollower, tVerified=xtVerified, tDescription=xtDescription, tFollowerCount=xtFollowerCount, tTimezone=xtTimezone)     
		    new.save()
                    newSharer = str(new)[15:-1]
		    return newSharer
