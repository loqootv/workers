#!/usr/bin/env python

import soundcloud


def scINFO(url):
    client = soundcloud.Client(client_id='8999040f26b6ad08740c3980f9cf0e29')
    track_url = url 
    track = client.get('/resolve', url=track_url)
    scID = track.id
    scUSER_PIC = track.user['avatar_url']
    scUSER_LINK = track.user['permalink_url']
    scTAGS = track.tag_list
    scTITLE = track.title
    try:
    	      scDOWNLOAD = track.download_url
    except AttributeError:
	      scDOWNLOAD = None 
    scDURATION = track.duration
    try:
	    scPURCHASE = track.purchase_url
    except AttributeError:
	    scPURCHASE = None
    scARTWORK = track.artwork_url
    scSPINS = track.playback_count
    scLIKES = track.favoritings_count
    scDESCRIP = track.description
    users = client.get('/resolve',url=scUSER_LINK )
    scUSER_CITY = users.city
    scUSER_COUNTRY = users.country
    try:
      scUSER_WEBPAGE = users.website
    except AttributeError:
      scUSER_WEBPAGE = None	   
    scUSER_TRACK_COUNT = users.track_count
    return scID, scDURATION, scUSER_CITY, scUSER_COUNTRY, scTITLE, scUSER_LINK, scUSER_PIC, scARTWORK, scDESCRIP, scDOWNLOAD, scLIKES, scSPINS, scPURCHASE, scTAGS, scUSER_WEBPAGE, scUSER_TRACK_COUNT 


