#!/usr/bin/env python 

import requests
import urlparse as ups
from soundcloudAPI import *
import isodate
import os

class contentProviders():
	

	def parseYoutubeUrl(self, url):                                                                                                                                               
         m = ups.urlparse(url)
         m1 = ups.parse_qs(m.query)['v']
         m1a  = str(m1)[2:-2]    
         return m1a         

	def youtube(self, url): 
         YTAPIKEY = "AIzaSyBl_vevuT3hzLCrK4rysf3XMPZtt5LxnWg"  
         ytID = self.parseYoutubeUrl(url)  
         ytNOEMBED = requests.get("http://noembed.com/embed?url=http://www.youtube.com/watch?v=%s"% ytID).json() 
         ytContentDetails = requests.get("https://www.googleapis.com/youtube/v3/videos?part=contentDetails,+statistics,+snippet&id=%s&key=%s"%(ytID, YTAPIKEY)).json()      
         ytDUR = ytContentDetails['items'][0]['contentDetails']['duration'] 
         ytViews = ytContentDetails['items'][0]['statistics']['viewCount']  
         ytLikes = ytContentDetails['items'][0]['statistics']['likeCount']  
         ytDislikes = ytContentDetails['items'][0]['statistics']['dislikeCount']  
         ytDesc = ytContentDetails['items'][0]['snippet']['description']  
         ytChannelTitle = ytContentDetails['items'][0]['snippet']['channelTitle']  
         ytChannelDetails = requests.get("https://www.googleapis.com/youtube/v3/channels?part=snippet,+contentDetails,+statistics&forUsername=%s&key=%s"%(ytChannelTitle, YTAPIKEY)).json()      
         ytCreatorProfileImageUrl = ytChannelDetails['items'][0]['snippet']['thumbnails']['high']['url'] 
	 try:
         	ytCreatorGooglePlusId = ytChannelDetails['items'][0]['contentDetails']['googlePlusUserId'] 
	 except IndexError:
		ytCreatorGooglePlusId = ""
         ytCreatorsSubscriberCount = ytChannelDetails['items'][0]['statistics']['subscriberCount'] 
         self.ytTime = isodate.parse_duration(ytDUR).total_seconds()  
	 contentCreatorUsername = ytNOEMBED['author_name']
	 contentCreatorProfileUrl = ytNOEMBED['author_url']
	 contentImageUrl = ytNOEMBED['thumbnail_url']
	 contentTitle = ytNOEMBED['title']
         meta = {'contentDuration': self.ytTime, 'contentViews': ytViews, 'contentLikes': ytLikes, 'contentDislikes': ytDislikes, 'contentType': "youtube", 'contentId': ytID, 'contentUrl': url } 
	 return meta 
         #return ytDUR, ytViews, ytLikes, ytDislikes, ytDesc, ytChannelTitle, ytCreatorsSubscriberCount, ytCreatorProfileImageUrl, ytCreatorsSubscriberCount, ytCreatorGooglePlusId, contentCreator, contentProfile, contentArtUrl, contentTitle 

#        def soundcloud(self, url):
#		scID, scDURATION, scUSER_CITY, scUSER_COUNTRY, scTITLE, scUSER_LINK, scUSER_PIC, scARTWORK, scDESCRIP, scDOWNLOAD, scLIKES, scSPINS, scPURCHASE, scTAGS, scUSER_WEBPAGE, scUSER_TRACK_COUNT = scINFO(url)
#		scSECONDS = float(scDURATION) / 1000
#		meta = {'contentType': "soundcloud",'contentId': scID, 'contentDuration': scSECONDS, 'contentCreatorCity': scUSER_CITY, 'contentCreatorCountry': scUSER_COUNTRY, 'contentTitle': scTITLE, 'contentImageUrl': scARTWORK, 'contentDesc': scDESCRIP, 'contentCreatorProfileUrl': scUSER_LINK, 'contentCreatorProfileImageUrl': scUSER_PIC, 'contentUrl': url, 'contentLikes': scLIKES, 'contentViews': scSPINS, 'contentPurchaseLink': scPURCHASE, 'contentHashtags': scTAGS, 'contentCreatorWebsite': scUSER_WEBPAGE, 'scUSER_TRACK_COUNT': ``}
 #        	return meta 
	  
       	def mixcloud(self, url, sharer, show, access=0):
		step1 = url.split("www.")[0]
		step2 = "http://api." + str(step1)
		step3 = request.get("step2").json
		contentDuration = step2['audio_length']
		contentDesc = step3['description']
		contentLikes = step3['favorite_count']
		contentPlayUrl = step3['key']
		contentTitle = step3['name']
		contentImageUrl = step3['pictutes']['large']
		contentViews = step3['play_count']
		contentHashtags = []
		try:
			tag0 = step3['tags'][0]['name'] 
			contentHashtags.append(tag0)
		except IndexError:
			tag0 = ""
		try:
			tag1 = step3['tags'][1]['name'] 
			contentHashtags.append(tag1)
		except IndexError:
			tag1 = ""
		try:
			tag2 = step3['tags'][2]['name'] 
			contentHashtags.append(tag2)
		except IndexError:
			tag2 = ""
		try:
			tag3 = step3['tags'][3]['name'] 
			contentHashtags.append(tag3)
		except IndexError:
			tag3 = ""
		try:
			tag4 = step3['tags'][4]['name'] 
			contentHashtags.append(tag4)
		except IndexError:
			tag4 = ""
		try:
			tag5 = step3['tags'][5]['name'] 
			contentHashtags.append(tag5)
		except IndexError:
			tag5 = ""
		contentCreatorFullName = step3['user']['name']
		contentCreatorProfileImageUrl = step3['user']['pictures']['large']
		contentCreatorProfileImageUrls = step3['user']['pictures']['medium_mobile']
		contentCreatorProfileUrl = step3['user']['url']
		contentCreatorUsername = step3['user']['username']
		step4  = requests.get("http://api.mixcloud.com/" + contentCreatorUsername)
 #       def facebook(self, show):
#		show['data'][0]['caption'].encode('utf-8')
#		contentDesc = show['data'][0]['description'].encode('utf-8')
#		show['data'][0]['created_time'].encode('utf-8')
#		show['data'][0]['link']
#		contentTitle = show['data'][0]['name'].encode('utf-8')
#		show['data'][0]['message'].encode('utf-8')
#		show['data'][0]['picture']
#		sharerfbId = show['data'][0]['from']['id'].encode('utf-8')
#		show['data'[0]['from']['name'].encode('utf-8')
	        #sharer = graph.get('%s?fields=picture.width(800).height(800),id,name,first_name,middle_name,last_name,gender,locale,link,username,updated_time'% sharerfbID)
		#fbGender = sharer['gender']
		#sharerOid = sharer.addFacebook()
		#showOid = show.addShow()
		#creatorOid = creator.addCreator()
		#shareOid = share.addShare()

		#return contentUrl + "shareid=" + shareOid
	def instagram(self, url):
		 INSTAACCESSTOKEN = os.environ['INSTAACCESSTOKEN']
		 shortcode = str(url.split("p/")[1])[0:10]
		 instaSTEP2 = requests.get("http://api.instagram.com/oembed?url=http://instagr.am/p/%s/"% shortcode).json() 
		 instaMeID = instaSTEP2['media_id']
		 instaINFO = requests.get("https://api.instagram.com/v1/media/%s?access_token=%s" % (instaMeID, INSTAACCESSTOKEN)).json()
		 contentUrl = str(instaINFO['data']['images']['low_resolution']['url'].encode('utf-8'))
	         contentTitle = str(instaINFO['data']['caption']['text'].encode('utf-8')) 
	         contentLikes = instaINFO['data']['likes']['count']
		 contentLikers = []
	         #contentHashtags
		 #contentCreatorFullName
		 #contentCreatorProfileImageUrl
		 #contentCreatorUsername
		 #contentCreatorWebsite
		 #contentCreatorFollowerCount
		 #contentCreatorContentCount
		 instaUID = instaINFO['data']['user']['id']
		 instaUSER = requests.get("https://api.instagram.com/v1/users/%s?access_token=%s" % (instaUID, INSTAACCESSTOKEN)).json()
		 #for msgkey, subdict, instakey in (('profilePIC', 'user', 'profile_picture'), 
		#		    ('contentCreator', 'user', 'username'),
		#		    ('contentDesc', 'caption', 'text'),
		#		    ('contentUrl','images', 'standard_resolution')):
		 #    try:               
		#	 loqootvMSG[msgkey] = instaINFO['data'][subdict][instakey]
		 #    except TypeError:
		#	 loqootvMSG[msgkey] = ""
		 #loqooTVSHOW = json.dumps(loqootvMSG)
		 return contentUrl, instaMeID
#	contentCreator = 
#	contentTitle = 
#	contentImageUrl = 
#	ContentCreatorProfileImage = 	
	
