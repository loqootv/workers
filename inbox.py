 #!/usr/bin/python  
   
 ## Author: Alexander Melikian  
 ##  
 ## copyright 2013, Alexander Melikian  
 ##  
 ## This script checks for unread Facebook messages and displays them  
 ##  
   
from facepy import GraphAPI  
   
def get_name_from_uid(author_id):  
	   query_string = "SELECT name FROM user WHERE uid = " + str(author_id)  
	   author = graph.fql(query_string)  
	   if author['data']:  
	     return author['data'][0]['name']  
	   else:  
	     return "Invalid ID"  
   
def print_recipients(recipients, message):  
	   recipients.remove(message['snippet_author'])  
	   for recipient in recipients:  
	     print "  " + get_name_from_uid(recipient)  
   
def get_messages(message_list):  
	   for message in message_list:  
	     if message:  
	       print "FROM"  
	       print "  " + get_name_from_uid(message['snippet_author'])  
	   
	       print "RECIPIENT(S)"  
	       print_recipients(message['recipients'], message)  
		 
	       print "BODY: " + message['snippet']  
	       print "-" * 40 + '\n'  
	   
	 # Initialize the Graph API with a valid access token  
oauth_access_token = 'CAATp7ceWZC18BABJ8GDgvN7zRJYWY3ZBN4QqeVD8BcxIj0xSOCXnp81r8DSeZB1y9Cn75q1kpUZAcASr4ZCh6F3B69UEYdZBpvfZB2bkPxf2mZAy9L18kdjoquIMnQ1UmcfIfiTZAFa3JqlZA7gtZCw5W9ZCmFKwUfWNnP9Kq8kJUM5pFmon9BjxjdKBq93PLZCWnzAX0OKvW2CXD4wZDZD'  
graph = GraphAPI(oauth_access_token)  
	   
	 # FQL: get all messages from inbox  
fql_get_messages = graph.fql("SELECT snippet, timestamp, snippet_sender, snippet_message_id FROM unified_thread WHERE folder='inbox'  AND 'inbox'")  
	   
	 # Get the data of the messages in a list  
message_list = fql_get_messages['data']  
	   
	 # print the messages 
if message_list:  
    get_messages(message_list)  
else:  
    print "Inbox is Empty"  
