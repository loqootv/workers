#!/usr/bin/env python

import subprocess
from subprocess import call

def restartltv():
	subprocess.Popen(["/Users/administrator/LOQOOTV/scripts/restartld", "tv.loqoo.1.loop"])
	subprocess.Popen(["/Users/administrator/LOQOOTV/scripts/restartld", "tv.loqoo.1.api"])

def stopapi():
	subprocess.Popen(["launchctl", "remove", "tv.loqoo.1.api"])

def delads():
	subprocess.Popen(["/Users/administrator/delads"])
		
def delqueue():
	subprocess.Popen(["/usr/local/bin/redis-cli", "del", "tv.loqoo.1.queue"])

def delrtq():
	subprocess.Popen(["/usr/local/bin/redis-cli", "del", "tv.loqoo.1.queue.realtime"])

def delvipq():
	subprocess.Popen(["/usr/local/bin/redis-cli", "del", "tv.loqoo.1.queue.vip"])

def restartloop():
	subprocess.Popen(["/Users/administrator/LOQOOTV/scripts/restartld", "tv.loqoo.1.loop"])

def restartfeeda():
	subprocess.Popen(["/Users/administrator/LOQOOTV/scripts/restartld", "tv.loqoo.1.feedA"])

def restartstream():
	subprocess.Popen(["/Users/administrator/LOQOOTV/scripts/restartld", "tv.loqoo.1.startFMLE"])

def stopltv():
	subprocess.Popen(["launchctl", "remove", "tv.loqoo.1.loop"])
        subprocess.Popen(["launchctl", "remove", "tv.loqoo.1.api "])

def playad1():
        pass

def playad2():
        pass

def playad3():
        pass

def playad4():
        pass

def playad5():
	pass

def killinsta():
	subprocess.Popen(["/Users/administrator/LOQOOTV/scripts/killInsta.sh"])
