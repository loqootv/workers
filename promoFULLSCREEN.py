#!/usr/bin/env python

import sys
from PySide import QtCore, QtGui, QtOpenGL
from PySide.QtWebKit import QGraphicsWebView
 
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
  
    width = 900
    height = 900 
 

    scene = QtGui.QGraphicsScene()
    
    view = QtGui.QGraphicsView(scene)
    
    #scene.setSceneRect(50,50,50,50)
    scene.setBackgroundBrush(QtCore.Qt.black)
    view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    view.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
    view.setAttribute(QtCore.Qt.WA_TranslucentBackground)
    view.setStyleSheet("background:transparent;")

    webviewMAIN = QGraphicsWebView()
    #webviewTOP = QGraphicsWebView()
    #webviewBOTTOM = QGraphicsWebView()
    # webviewRIGHT1 = QGraphicsWebView()
    #webviewRIGHT2 = QGraphicsWebView()
    url = sys.argv[1]
    #url2 = sys.argv[2]
    #url3 = sys.argv[3]
    #url4 = sys.argv[4]
    #url5 = sys.argv[5]
    #webview.setResizesToContents(enabled)
    webviewMAIN.load(QtCore.QUrl("%s" % url))
    webviewMAIN.setPos(-10,-0)
    webviewMAIN.setZoomFactor(1)
    webviewMAIN.resize(1500, 1000)
    #webviewTOP.setPos(-10,-200)
    #webviewTOP.resize(1500, 150 )
    #webviewTOP.load(QtCore.QUrl("%s" % url2))
    #webviewRIGHT2.setPos(1090,-3)
    #webviewRIGHT2.load(QtCore.QUrl("%s" % url3))
    #webviewRIGHT2.resize(300, 300)
 #  webviewBOTTOM.resize(1225, 150)
    scene.addItem(webviewMAIN) 

    view.resize(width, height)
    view.showFullScreen()
    view.raise_()
    view.show()
 
    sys.exit(app.exec_())
