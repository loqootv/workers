#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PySide import QtGui, QtCore
from PySide.QtWebKit import QGraphicsWebView

class Example(QtGui.QWidget):
    
    def __init__(self):
        super(Example, self).__init__()
        
        self.initUI()

    def initUI(self):
        
        self.scene = QtGui.QGraphicsScene()
        self.view = QtGui.QGraphicsView(self.scene, self)
        self.view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.webview = QGraphicsWebView()
        url = sys.argv[1]
        self.webview.load(QtCore.QUrl("%s" % url))
        self.webview.setZoomFactor(1.0) 
        self.scene.addItem(self.webview) 


        self.setGeometry(300, 300, 350, 350)
        self.setWindowTitle('Icon')
        self.setWindowIcon(QtGui.QIcon('web.png'))        
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint) 
        self.showFullScreen()
        self.center() 
        self.setWindowTitle('LTV')
        self.show()
        

    def center(self):
        
        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

def main():
    
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
