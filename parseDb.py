#!/usr/bin/env python

from parse_rest.connection import register
from parse_rest.datatypes import Object
from parse_rest.query import *
import os, json, httplib, urllib, sys

PARSEAPPID = os.environ['PARSEAPPID']
PARSERESTAPIKEY = os.environ['PARSERESTAPIKEY']

register(PARSEAPPID, PARSERESTAPIKEY)

class Sharer(Object):
     
    def addFacebook(self,xfbUsername, xfbId, xfbProfileImageUrl, xtLocale, xtBackgroundImageUrl, xtVerified, xtDescription, xtFollowerCount, xtFirstName, xtLastName):
            """add sharer of loqootv shows toi ParseDB  datastore.
               data includes sharers social network profile info.
               how many shares on loqootv, its shared show's object ids.
               """
	    try:
		    try:
                      connection = httplib.HTTPSConnection('api.parse.com', 443)
                      params = urllib.urlencode({"where":json.dumps({
                           "tScreename": xtScreename,
                          })})
                      connection.connect()
                      connection.request('GET', '/1/classes/Sharer?%s' % params, '', {
                           "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
                           "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH"
                          })
                      oid = json.loads(connection.getresponse().read())
		      ooid = oid['results'][0]['objectId']
                    except IndexError:
		      connection = httplib.HTTPSConnection('api.parse.com', 443)
                      connection.connect()
		      connection.request('POST', '/1/classes/Sharer/', json.dumps({
		              "shares": {
        			      "__op": "Increment",
			              "amount": 1
				      }, 
		              "tBackgroundImageUrl": xtBackgroundImageUrl,
			      "tDescription": xtDescription,
                              "tFollowerCount": xtFollowerCount,
                              "tId": xtId,
                              "tLang": xtLang,
                              "tLocation": xtLocation,
			      "tProfileImageUrl": xtProfileImageUrl,
			      "tScreename": xtScreename,
			      "tTimeZone": xtTimezone,
			      "tVerified": xtVerified,
			      "tFirstName": xtFirstName,
			      "tLastName": xtLastName
			    }),  {
			      "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			      "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			      "Content-Type": "application/json"
			    })
	              result = json.loads(connection.getresponse().read())
		      return str(result['objectId']) 
		    connection = httplib.HTTPSConnection('api.parse.com', 443)
                    connection.connect()
		    connection.request('PUT', '/1/classes/Sharer/%s'% ooid, json.dumps({
			    "tProfileImageUrl": xtProfileImageUrl,
			    "tLocation": xtLocation,
			    "tTimezone": xtTimezone,
			    "tBackgroundImageUrl": xtBackgroundImageUrl,
			    "tDescription": xtDescription,
			    "tFollowerCount": xtFollowerCount,
			    "shares": {
				    "__op": "Increment",
				    "amount": 1
				    }
			  }),  {
			    "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			    "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			    "Content-Type": "application/json"
			  })
	            result = json.loads(connection.getresponse().read())
		    return str(ooid)
            except QueryResourceDoesNotExist:
		    connection = httplib.HTTPSConnection('api.parse.com', 443)
                    connection.connect()
		    connection.request('POST', '/1/classes/Sharer/', json.dumps({
		            "share": {
        			    "__op": "Increment",
			            "amount": 1
				    }, 
		            "tBackgroundImageUrl": xtBackgroundImageUrl,
			    "tDescription": xtDescription,
                            "tFollowerCount": xtFollowerCount,
                            "tId": xtId,
                            "tLang": xtLang,
                            "tLocation": xtLocation,
			    "tProfileImageUrl": xtProfileImageUrl,
			    "tScreename": xtScreename,
			    "tTimeZone": xtTimezone,
			    "tVerified": xtVerified
			  }),  {
			    "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			    "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			    "Content-Type": "application/json"
			  })
	            result = json.loads(connection.getresponse().read())
		    return str(result['objectId']) 

    def addTwitter(self,xtScreename, xtId, xtProfileImageUrl, xtLocation, xtTimezone, xtLang, xtBackgroundImageUrl, xtVerified, xtDescription, xtFollowerCount, xtFirstName, xtLastName):
            """add sharer of loqootv shows toi ParseDB  datastore.
               data includes sharers social network profile info.
               how many shares on loqootv, its shared show's object ids.
               """
	    try:
		    try:
                      connection = httplib.HTTPSConnection('api.parse.com', 443)
                      params = urllib.urlencode({"where":json.dumps({
                           "tScreename": xtScreename,
                          })})
                      connection.connect()
                      connection.request('GET', '/1/classes/Sharer?%s' % params, '', {
                           "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
                           "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH"
                          })
                      oid = json.loads(connection.getresponse().read())
		      ooid = oid['results'][0]['objectId']
                    except IndexError:
		      connection = httplib.HTTPSConnection('api.parse.com', 443)
                      connection.connect()
		      connection.request('POST', '/1/classes/Sharer/', json.dumps({
		              "shares": {
        			      "__op": "Increment",
			              "amount": 1
				      }, 
		              "tBackgroundImageUrl": xtBackgroundImageUrl,
			      "tDescription": xtDescription,
                              "tFollowerCount": xtFollowerCount,
                              "tId": xtId,
                              "tLang": xtLang,
                              "tLocation": xtLocation,
			      "tProfileImageUrl": xtProfileImageUrl,
			      "tScreename": xtScreename,
			      "tTimeZone": xtTimezone,
			      "tVerified": xtVerified,
			      "tFirstName": xtFirstName,
			      "tLastName": xtLastName
			    }),  {
			      "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			      "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			      "Content-Type": "application/json"
			    })
	              result = json.loads(connection.getresponse().read())
		      return str(result['objectId']) 
		    connection = httplib.HTTPSConnection('api.parse.com', 443)
                    connection.connect()
		    connection.request('PUT', '/1/classes/Sharer/%s'% ooid, json.dumps({
			    "tProfileImageUrl": xtProfileImageUrl,
			    "tLocation": xtLocation,
			    "tTimezone": xtTimezone,
			    "tBackgroundImageUrl": xtBackgroundImageUrl,
			    "tDescription": xtDescription,
			    "tFollowerCount": xtFollowerCount,
			    "shares": {
				    "__op": "Increment",
				    "amount": 1
				    }
			  }),  {
			    "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			    "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			    "Content-Type": "application/json"
			  })
	            result = json.loads(connection.getresponse().read())
		    return str(ooid)
            except QueryResourceDoesNotExist:
		    connection = httplib.HTTPSConnection('api.parse.com', 443)
                    connection.connect()
		    connection.request('POST', '/1/classes/Sharer/', json.dumps({
		            "share": {
        			    "__op": "Increment",
			            "amount": 1
				    }, 
		            "tBackgroundImageUrl": xtBackgroundImageUrl,
			    "tDescription": xtDescription,
                            "tFollowerCount": xtFollowerCount,
                            "tId": xtId,
                            "tLang": xtLang,
                            "tLocation": xtLocation,
			    "tProfileImageUrl": xtProfileImageUrl,
			    "tScreename": xtScreename,
			    "tTimeZone": xtTimezone,
			    "tVerified": xtVerified
			  }),  {
			    "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			    "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			    "Content-Type": "application/json"
			  })
	            result = json.loads(connection.getresponse().read())
		    return str(result['objectId']) 
    def addShow(self, show, sharer):
            """add Sharer's shared Show's objectId to Sharer's ParseDB
               datastore entry.
               """
            connection = httplib.HTTPSConnection('api.parse.com', 443)
            connection.connect()
            connection.request('PUT', '/1/classes/Sharer/%s'% sharer, json.dumps({
                  "shows": {
                  "__op": "AddUnique",
                  "objects": [
                       show,
                     ]
                  },
                }), {
                  "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
                  "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
                  "Content-Type": "application/json"
                })
            result = json.loads(connection.getresponse().read())
            print result 

class Show(Object):
    def add(self, sharer, contentUrl):
            """add shared Shows to ParseDB datastore
   
               if show is already in DB ( checked by its contentUrl, then
               add sharer to entry,
               """
	    try:
		    try:
                      connection = httplib.HTTPSConnection('api.parse.com', 443)
                      params = urllib.urlencode({"where":json.dumps({
                           "contentUrl": contentUrl,
                          })})
                      connection.connect()
                      connection.request('GET', '/1/classes/Show?%s' % params, '', {
                           "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
                           "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH"
                          })
                      oid = json.loads(connection.getresponse().read())
		      ooid = oid['results'][0]['objectId']
                    except IndexError:
		      connection = httplib.HTTPSConnection('api.parse.com', 443)
                      connection.connect()
		      connection.request('POST', '/1/classes/Show/', json.dumps({
		            "sharer": {
		              "__type": "Pointer",
			      "className": "Sharer",
			      "objectId": sharer
                            },
		            "shared": {
                            "__op": "Increment",
	                    "amount": 1
                            },
			    "lastSharer": sharer, 
			    "contentUrl": contentUrl
			  }),  {
			    "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			    "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			    "Content-Type": "application/json"
			  })
	              result = json.loads(connection.getresponse().read())
		      return str(result['objectId']) 
		    connection = httplib.HTTPSConnection('api.parse.com', 443)
                    connection.connect()
		    connection.request('PUT', '/1/classes/Show/%s'% ooid, json.dumps({
			    "otherSharers": {
			    "__op": "AddUnique",
			    "objects": [
			    	sharer
				]
			      },
			    "lastSharer": sharer,
		            "shared": {
                            "__op": "Increment",
	                    "amount": 1
                              } 
			  }),  { 
			    "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			    "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			    "Content-Type": "application/json"
			  })
	            result = json.loads(connection.getresponse().read())
		    return str(ooid)
	    except QueryResourceDoesNotExist:
		    connection = httplib.HTTPSConnection('api.parse.com', 443)
                    connection.connect()
		    connection.request('POST', '/1/classes/Show/', json.dumps({
		            "sharer": {
		              "__type": "Pointer",
			      "className": "Sharer",
			      "objectId": sharer
			    },
		            "shared": {
                            "__op": "Increment",
	                    "amount": 1
                              }, 
			    "contentUrl": contentUrl
			  }),  {
			    "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			    "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			    "Content-Type": "application/json"
			  })
	            result = json.loads(connection.getresponse().read())
		    return result 

    def queryShowById(self, objId):
            """ query show by ParseDb objectId

            returns all attributes related to the 
            the show's metadata, acquired from the show's
            content provider.
            returns one entry
            """
	    try:
	      connection = httplib.HTTPSConnection('api.parse.com', 443)
	      params = urllib.urlencode({"where":json.dumps({
		   "objectId": objId,
		  })})
	      connection.connect()
	      connection.request('GET', '/1/classes/Show?%s' % params, '', {
		   "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
		   "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH"
		  })
	      oid = json.loads(connection.getresponse().read())
	      return oid
            except:
              pass  

    def addShowMeta(self, objId, xcontentProvider, xcontentTitle, xcontentDescrp, xcontentDuration, xcontentId, xcontentLikes, xcontentViews, xcontentDislikes, xcontentImageUrl, sharer ):
	    """ add attributes to a Show, acquired from
            it's content provider.
             """ 
            try:
		    connection = httplib.HTTPSConnection('api.parse.com', 443)
		    connection.connect()
		    connection.request('PUT', '/1/classes/Show/%s'% objId, json.dumps({
				"contentProvider": xcontentProvider,
				"contentTitle": xcontentTitle,
          		        "contentDescription": xcontentDescrp, 
				"contentDuration": xcontentDuration,
				"contentId": xcontentId,
				"contentLikes": xcontentLikes,
				"contentViews": xcontentViews,
				"contentDislikes": xcontentDislikes,
				"contentImageUrl": xcontentImageUrl,
				"lastSharer": sharer
			       
			  }),  { 
			    "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
			    "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
			    "Content-Type": "application/json"
			  })
	    	    result = json.loads(connection.getresponse().read())
		    return result
	    except:
    		print "Unexpected error:", sys.exc_info()[0]
    		raise

class Creator(Object):

    def add():			
	pass

class Scene(Object):

    def addScene(sceneType, sceneUrl, sceneName, networkName, networkEmail, sceneType, sceneDescp, sceneTag1, sceneTag2, scenePrice, scenePriceDnom, sendtoChannel, networkImage):

	connection = httplib.HTTPSConnection('api.parse.com', 443)
	connection.connect()
	connection.r
	action = {'originalAudio': {'tv.loqoo.v4.ORIGINAL_VIDEO_SCENE'}, 'originalVideo': {'tv.loqoo.v4.ORIGINAL_AUDIO_SCENE'},
			'originalImage': {'tv.loqoo.v4.ORIGINAL_IMAGE_SCENE'}, 'originalScene': {'tv.loqoo.v4.ORIGINAL_SCENE'},
			'thirdPartyAudio': {'tv.loqoo.v4.THIRDPARTY_AUDIO_SCENE'}, 'thirdPartyVideo': {'tv.loqoo.v4.THIRD_VIDEO_SCENE'},
			'thirdPartyImage': {'tv.loqoo.v4.THIRDPARTY_IMAGE_SCENE'}, 'thirdPartyPlayStore': {'tv.loqoo.v4.THIRDPARTY_PLAYSTORE'},
			'thirdPartyWeb': {'tv.loqoo.v4.THIRDPARTY_WEB_SCENE'}, 'convo': {'tv.loqoo.v4.CONVO'}}
	handler = action.get(sceneType)
	request('POST', '/1/push', json.dumps({
	       "channels": [
		  sendToChannel 
	       ],
	       "data": {
		 "action": handler,
		 "sceneUrl": url,
		 "sceneName": sceneName,
		 "networkName": networkName,
		 "networkEmail": networkEmail,
		 "sceneType": sceneType,
		 "sceneDescp": sceneDescp,
		 "sceneTag1": sceneTag1,
		 "sceneTag2": sceneTag2,
		 "scenePrice": scenePrice,
		 "scenePriceDnom": scenePriceDnom,
		 "sendToChannel": sendToChannel,
		 "networkImage": networkImage,
	       }
	     }), {
	       "X-Parse-Application-Id": "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab",
       "X-Parse-REST-API-Key": "bcgTZa4c7pxyls9k1r59X3B5s4kjF7kofH76nCiH",
       "Content-Type": "application/json"
	     })
	result = json.loads(connection.getresponse().read())
	print result
